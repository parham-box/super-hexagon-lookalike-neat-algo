function nextGeneration() {
  calculateFitness();
  for (let i = 0; i < POPULATION; i++) {
    players[i] = pickOne();
  }
  savedPlayers = [];
}

function calculateFitness() {
  let sum = 0;
  for(let i = 0; i < savedPlayers.length; i++){
    sum += savedPlayers[i].scoree;
  }
  for(let i = 0; i < savedPlayers.length; i++){
    savedPlayers[i].fitness = savedPlayers[i].scoree / sum;
  }
}
function pickOne(){
  var index = 0;
  var r = random(1);
  while(r>0){
    r = r - savedPlayers[index].fitness;
    index++;
  }
  index--;
  let player = savedPlayers[index];
  let child = new Player(player.brain);
  child.mutate();
  return child;

}
