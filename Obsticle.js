class Obsticle {
  constructor(start, r) {
    this.howMuch = 2 * PI / 6;
    this.start = start;
    this.r = r;
    this.end = start + (2 * PI - this.howMuch);
  }

  show() {
    noFill();
    // console.log('as');
    arc(width / 2, height / 2, this.r, this.r, this.start, this.end);
  }

  update(stop, rate) {

    this.show();
    // println(rate);
    if (!stop) {
      this.r -= rate;
    }
  }

}
