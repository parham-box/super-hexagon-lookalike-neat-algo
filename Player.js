class vector {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class Player {

  constructor(brain) {
    this.r = 50;
    this.point = new vector(width / 2 - 25, height / 2 - 25);
    this.center = new vector(width / 2, height / 2);
    this.deltaX = this.center.x - this.point.x;
    this.deltaY = this.center.y - this.point.y;
    this.angle = Math.PI / 360;
    this.scoree = 0;
    this.fitness = 0;
    this.destination = 0;
    //find the radius of the circle
    this.radius = dist(this.center.x, this.center.y, this.point.x, this.point.y);
    if (brain) {
      this.brain = brain.copy();
    } else {
      this.brain = new NeuralNetwork(3, 3, 2);
    }
  }
  mutate() {
    this.brain.mutate(0.1);
  }

  show() {
    circle(width / 2, height / 2, this.r);
    //find the point based on the angle
    let x = this.center.x + cos(this.angle) * this.radius;
    let y = this.center.y + sin(this.angle) * this.radius;
    //draw the traveling point
    ellipse(x, y, 8, 8);
    //triangle(x1, y1, x2, y2, x3, y3);
  }
  update(obs) {
    this.scoree++;
    let newAngle;
    if (this.angle < 0) {
      newAngle = 2 * PI + this.angle;
    } else {
      newAngle = this.angle;
    }

    let inputs = [];
    inputs[0] = (newAngle) / (PI * 2);
    inputs[1] = (obs.start - PI / 24) / PI * 2;
    inputs[2] = (obs.end + PI / 24) / PI * 2;
    let outputs = this.brain.predict(inputs);
    let dest = outputs[0] * 2 * PI;

    console.log(dest);
      if (outputs[1] > 0.5) {
        this.moveLeft(dest);
      } else {
        this.moveRight(dest);
      }
  }


  playerPos() {
    //return new PVector(center.x + cos(angle)*radius,center.y + sin(angle)*radius);
    return this.angle;
  }

  moveLeft(dest) {
    //increment the angle to move the point
    // if (this.travel <= 0) {
    if (dest > PI/24) {
      if (this.angle < -2 * PI) {
        this.angle = 0;
      } else {
        this.angle -= PI / 360;
      }
      dest -= PI/24;
    }
    //   this.travel -= PI / 360;
    // }
  }

  check(start, end) {
    //println(angle);
    //println(start);
    //println(end);
    //println("--------");
    let newAngle;
    if (this.angle < 0) {
      newAngle = 2 * PI + this.angle;
    } else {
      newAngle = this.angle;
    }
    if (abs(newAngle) > start - PI / 24 && abs(newAngle) < end + PI / 24) {
      return true;
    }
    noFill();
    return false;
  }

  moveRight(dest) {
    //increment the angle to move the point
    // if (this.travel <= 0) {
    dest = -dest;
    if (dest < -PI/24) {
      if (this.angle > 2 * PI) {
        this.angle = 0;
      }else {
        this.angle += PI / 360;
      }
      dest += PI/24;

    }
    //   this.travel -= PI / 360;
    //
    // }
  }
}
