const POPULATION = 50;
let obsticles = [];
let score = 0;
let rate = 5;
let players = [];
let savedPlayers = [];
let slider;
let gen = 1;

function setup() {
  createCanvas(800, 800);
  // slider = createSlider(1,100,1);
  frameRate(300); //increase this to make the dots go faster
  for (let i = 0; i < POPULATION; i++) {

    players.push(new Player());
  }
  obsticles.push(new Obsticle(random(2 * PI), 1000));


  // let nn = new NeuralNetwork(2, 2, 1);
  // for (let i = 0; i < 500000; i++) {
  //     let data = random(train_data);
  //     nn.train(data.inputs, data.targets);
  //
  // }
  // console.log(nn.predict([0, 1]));
  // console.log(nn.predict([1, 0]));
  // console.log(nn.predict([0, 0]));
  // console.log(nn.predict([1, 1]));


}


function draw() {
  background(255);

  // for (let x = 0; x < slider.value; x++) {
    if (players.length === 0) {
      gen++;
      obsticles = [];
      obsticles.push(new Obsticle(random(2 * PI), 1000));
      console.log("generation: " + gen);
      nextGeneration();

    } else {
      for (let i = 0; i < players.length; i++) {

        players[i].show();
        players[i].update(obsticles[0]);
      }
      for (let i = 0; i < obsticles.length; i++) {
        obsticles[i].update(false, rate);
        if (obsticles[i].r < 100) {
          for (let i = 0; i < players.length; i++) {
            if (!(players[i].check(obsticles[0].start - obsticles[0].howMuch, obsticles[0].start))) {
              savedPlayers.push(players.splice(i, 1)[0]);
              continue;
            }
          }
          this.score++;
          // if (this.score != 1 && this.score != 0 && this.score % 2 == 0) {
          //   this.rate += 1.5;
          // }
          // console.log(this.score);
        }
        if (obsticles[i].r < 80) {
          obsticles.splice(i, 1);
        }
        if (obsticles[obsticles.length - 1].r < 600) {
          obsticles.push(new Obsticle(random(2 * PI), 1000));
        }
      }
    }
  // }
}


// function keyPressed() {
//     if (key == 'a') {
//         players[0].moveLeft();
//     } else if (key == 'd') {
//         players[0].moveRight();
//     }
// }
