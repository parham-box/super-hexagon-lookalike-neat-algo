# Super Hexagon Lookalike NEAT!

In This project I have used NeuroEvolution of Augmenting Topologies(NEAT) to make a Super Hexagon parody game to learn itself to play the game. This project uses no outside libraries, and everything was developed locally for it.

## Programming

This Project has been developed using P5.js library and JavaScript language.

## How to run it
Just open the index.html file in a browser.
